# Cambios 2.0.0 | 25/10/18

- Unificación de los componentes Input y TextoNumerico en Input.
- Ahora se puede especificar el tipo de Input como propiedad.
- Input detecta (según el type) si el elemento es númerico.
- Corrección del error "array" en VentanaModal.
- Correcciones menor de código en TextArea, Boton y Combo.