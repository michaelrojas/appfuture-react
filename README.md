# appfuture-react

> Librería con utilidades y controles de usuario para proyectos de Appfuture

[![NPM](https://img.shields.io/npm/v/appfuture-react.svg)](https://www.npmjs.com/package/appfuture-react) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save appfuture-react
```

## Usage

```jsx
import React, { Component } from 'react'

import { MyComponent } from 'appfuture-react'

class Example extends Component {
  render () {
    return (
      <MyComponent/>
    )
  }
}
```

## License

MIT © [Appfuture Group SAS](https://github.com/Appfuture Group SAS)
